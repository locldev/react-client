import React, {useEffect,useContext} from 'react';
import {BrowserRouter, Route, Switch, Redirect} from "react-router-dom";
import Home from './components/pages/Home'
import Registration from './components/pages/Registration'
import Login from './components/pages/Login'
import NavBar from './components/template/NavBar';
import PasswordReset from './components/pages/PasswordReset';
import Profile from './components/pages/Profile';
import ForgotPassword from './components/pages/ForgotPassword';
import axios from 'axios';
import {config} from './config';
import AuthContext from './context/AuthContext';
import './App.css';

const App = () => {
    const {auth,setAuthData} = useContext(AuthContext);
    const refreshAccessToken = async () => {
        const refreshToken = localStorage.getItem("refresh_token");
        if (!auth.accessToken && refreshToken) {
            const options = {
                headers: {Authorization: `Bearer ${refreshToken}`}
            }
            const res = await axios.post(config.SERVER_URL + '/api/auth/token/refresh', null, options);
            if (res.data.refresh_token) {
                setAuthData(res.data);
                return res.data;
            }
        }
    }

    axios.interceptors.response.use(function (response) {
        return response;
    }, async function (error) {
        const originalRequest = error.config;
        if (error.response.status === 401 && !originalRequest._retry) {
            originalRequest._retry = true;
            const refreshData = await refreshAccessToken();
            originalRequest.headers.Authorization = 'Bearer ' + refreshData.access_token;
            return axios.request(originalRequest);
        }
        return Promise.reject(error);
    });

    useEffect(refreshAccessToken, []);

    return (
        <>
            <BrowserRouter>
                    <NavBar/>
                    <div className="container">
                        <Switch>
                            <Route exact path="/"><Home/></Route>
                            {!auth.accessToken && <Route exact path="/registration"><Registration/></Route>}
                            <Route exact path="/password-reset/:token?"><PasswordReset/></Route>
                            {auth.accessToken && <Route exact path="/profile"><Profile/></Route>}
                            {!auth.accessToken && <Route exact path="/auth/verification/:token"><Home/></Route>}
                            {!auth.accessToken && <Route exact path="/auth/activation/:success"><Login/></Route>}
                            {!auth.accessToken && <Route exact path="/login"><Login/></Route>}
                            {!auth.accessToken && <Route exact path="/forgot-password"><ForgotPassword/></Route>}
                            <Redirect to="/"/>
                        </Switch>
                    </div>
            </BrowserRouter>
        </>
    );
}

export default App;
