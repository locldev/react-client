import { createContext, useState } from 'react'
const AuthContext = createContext()
export default AuthContext;

export const AuthProvider = ({ children }) => {
    const [auth, setAuth] = useState({})

    const setAuthData = (data)=>{
        localStorage.setItem('refresh_token', data.refresh_token)
        setAuth({
            username: data.username,
            userId: data.user_id,
            accessToken: data.access_token
        })
    }

    let contextData = {
        auth:auth,
        setAuthData: setAuthData,
    }
    return (
        <AuthContext.Provider value={contextData}>
            {children}
        </AuthContext.Provider>
    )
}