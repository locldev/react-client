import React, {useState} from 'react';
import axios from 'axios';
import {useForm} from "react-hook-form";
import * as yup from "yup";
import {yupResolver} from "@hookform/resolvers/yup";
import {config} from '../../config';


const ForgotPassword = () => {
    const [errorMessages, setErrorMessages] = useState([]);
    const [successMessages, setSuccessMessages] = useState([]);

    const schema = yup.object().shape({
        email: yup.string().required().email().label('Email'),
    });

    const {register, handleSubmit, formState: {errors}, reset} = useForm({
        resolver: yupResolver(schema),
    });
    const onSubmitHandler = (formData) => {
        setSuccessMessages([]);
        setErrorMessages([]);
        const data = JSON.stringify(formData);
        const options = {
            headers: {"content-type": "application/json"}
        }
        axios.post(config.SERVER_URL + '/api/auth/forgot-password', data, options)
            .then(function (res) {
                if (res.data.error) {
                    setErrorMessages([res.data.error]);
                }
                if (res.data.message) {
                    setSuccessMessages([res.data.message]);
                }
                reset();
            })
            .catch(function (error) {
                console.log(error);
            });
    };

    return (
        <div className="row">
            <div className="col-6 mx-auto mt-5">
                <h2>Forgot password</h2>
                {
                    successMessages.map(msg => (
                        <div className="alert alert-success" key={msg} role="alert">{msg}</div>
                    ))
                }
                {
                    errorMessages.map(error => (
                        <div className="alert alert-danger" key={error} role="alert">{error}</div>
                    ))
                }
                <form onSubmit={handleSubmit(onSubmitHandler)}>
                    <div className="mb-3">
                        <label htmlFor="email" className="form-label">Email address</label>
                        <input type="email" className="form-control" id="email" {...register("email")} required/>
                        <p className='text-danger'>{errors.email?.message}</p>
                    </div>
                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    )

}

export default ForgotPassword;