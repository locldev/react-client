import React, { useContext, useState, useEffect } from 'react';
import AuthContext from '../../context/AuthContext';
import axios from 'axios';
import { config } from '../../config';

const Profile = () => {

    const [user, setUser] = useState({});
    const {auth} = useContext(AuthContext);

    useEffect(() => getUserData(),[]);

    const getUserData = () => {
        const data = JSON.stringify({ id: auth.userId });
        const options = {
            headers: {
                "content-type": "application/json",
                'Authorization': 'Bearer ' + auth.accessToken
            }
        }
        axios.post(config.SERVER_URL + '/api/users/me', data, options)
            .then(function (res) {
                setUser(res.data);
            })
            .catch(function (error) {
                console.log(error);
            });
    }


        return (
            <div>
                <div className="col-3 mt-5">
                    <h2>Account info</h2>
                    <p>Some representative placeholder content for the three columns of text below the carousel. This is the first column.</p>
                    <ul className="list-group">
                        <li className="list-group-item">username: {user.username}</li>
                        <li className="list-group-item">email: {user.email}</li>
                    </ul>
                    <br />
                    <p><button className="btn btn-secondary" onClick={getUserData}>Load data »</button></p>
                </div>
            </div>
        )
}

export default Profile