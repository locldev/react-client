import React, { useState, useContext } from "react"
import { useParams, Redirect } from "react-router-dom";
import axios from 'axios';
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import AuthContext from '../../context/AuthContext';


const PasswordReset = () => {
    const { token } = useParams();
    const { auth } = useContext(AuthContext);
    const [errorMessages, setErrorMessages] = useState([]);
    const [successMessages, setSuccessMessages] = useState([]);
    const schema = yup.object().shape({
        password: yup.string().required().min(3).label('Password'),
        password_repeat: yup.string().required().label('Password'),
    });
    const { register, handleSubmit, reset } = useForm({
        resolver: yupResolver(schema),
    });


    if(!token && !auth) {
        return <Redirect to="/login" />
    }
    const onSubmitHandler = (formData) => {
        setSuccessMessages([]);
        setErrorMessages([]);
        const data = JSON.stringify(formData);
        const options = {
            headers: {
                "content-type": "application/json",
                'Authorization': 'Bearer ' + auth.accessToken
            }
        }
        axios.post('http://localhost:5000/api/auth/password/reset', data, options)
            .then(function (res) {
                if(res.data.error) {
                    setErrorMessages([res.data.error]);
                }
                if(res.data.message) {
                    setSuccessMessages([res.data.message]);
                    reset();
                    localStorage.removeItem("refresh_token");
                    console.log("user has bean log out");
                    window.location.replace("/login");
                } 

            })
            .catch(function (error) {
                setErrorMessages([error.response.data.message]);
            });

    };

    return (
        <div className="row">
            <div className="col-6 mx-auto mt-5">
                <h2>Password reset</h2>
                {
                    errorMessages.map(error => (
                        <div className="alert alert-danger" key={error} role="alert">{error}</div>
                    ))
                }
                {
                    successMessages.map(msg => (
                        <div className="alert alert-success" key={msg} role="alert">{msg}</div>
                    ))
                }
                <form onSubmit={handleSubmit(onSubmitHandler)}>
                    <input type="hidden" value={auth.userId} className="form-control" id="user_id" {...register("user_id")} />
                    <div className="mb-3">
                        <label for="password" className="form-label">Password</label>
                        <input type="password" className="form-control" id="password" {...register("password")} />
                    </div>

                    <div className="mb-3">
                        <label for="passwordRepeat" className="form-label">Repeat password</label>
                        <input type="password" className="form-control" id="password_repeat" {...register("password_repeat")} />
                    </div>
                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    )
}

export default PasswordReset