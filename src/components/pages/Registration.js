import React, { useState } from 'react'
import axios from 'axios';
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";

const Registration = () => {
    const [errorMessages, setErrorMessages] = useState([]);
    const [successMessages, setSuccessMessages] = useState([]);

    const schema = yup.object().shape({
        username: yup.string().required().min(3).max(30).label('Username'),
        full_name: yup.string().required().min(3).max(60).label('Full name'),
        email: yup.string().required().email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } }).label('Email'),
        password: yup.string().required().label('Password'),
    });
    const { register, handleSubmit, formState: { errors }, reset } = useForm({
        resolver: yupResolver(schema),
    });

    const onSubmitHandler = (formData) => {
        setSuccessMessages([]);
        setErrorMessages([]);
        const data = JSON.stringify(formData);
        const options = {
            headers: { "content-type": "application/json" }
        }
        axios.post('http://localhost:5000/api/auth/register', data, options)
            .then(function (res) {
                setSuccessMessages([res.data.message]);
                reset();
            })
            .catch(function (error) {
                setErrorMessages([error.response.data.message]);
            });
        
    };


    return (
        <div className="row">

            <div className="col-6 mx-auto mt-5">
                <h2>Registration</h2>
                {
                    errorMessages.map(error => (
                        <div className="alert alert-danger" key={error} role="alert">{error}</div>
                    ))
                }
                {
                    successMessages.map(msg => (
                        <div className="alert alert-success" key={msg} role="alert">{msg}</div>
                    ))
                }
                <form onSubmit={handleSubmit(onSubmitHandler)}>
                    <div className="mb-3">
                        <label htmlFor="username" className="form-label">Username</label>
                        <input type="text" className="form-control" {...register("username")} id="username" required/>
                        <p className='text-danger'>{errors.username?.message}</p>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="full-name" className="form-label">Full name</label>
                        <input type="text" className="form-control" id="full-name" {...register("full_name")} required/>
                        <p className='text-danger'>{errors.full_name?.message}</p>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="email" className="form-label">Email address</label>
                        <input type="email" className="form-control" id="email" {...register("email")} aria-describedby="emailHelp" required />
                        <div id="emailHelp" className="form-text">We'll never share your email with anyone else.</div>
                        <p className='text-danger'>{errors.email?.message}</p>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="password" className="form-label">Password</label>
                        <input type="password" className="form-control" id="password" {...register("password")} required/>
                        <p className='text-danger'>{errors.password?.message}</p>
                    </div>
                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>
            </div>

        </div>
    )
}

export default Registration
