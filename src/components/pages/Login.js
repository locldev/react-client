import React, { useState, useEffect, useContext } from 'react'
import { useParams, Link } from "react-router-dom";
import axios from 'axios';
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import AuthContext from '../../context/AuthContext';
import { config } from '../../config';


const Login = () => {
    const [errorMessages, setErrorMessages] = useState([]);
    const [successMessages, setSuccessMessages] = useState([]);
    const {setAuthData} = useContext(AuthContext);

    const schema = yup.object().shape({
        email: yup.string().required().email().label('Email'),
        password: yup.string().required().label('Password'),
    });

    const { register, handleSubmit, formState: { errors }, reset } = useForm({
        resolver: yupResolver(schema),
    });

    const onSubmitHandler = (formData) => {
        setSuccessMessages([]);
        setErrorMessages([]);
        const data = JSON.stringify(formData);
        const options = {
            headers: { "content-type": "application/json" }
        }
        axios.post(config.SERVER_URL + '/api/auth/login', data, options)
            .then(function (res) {
                localStorage.setItem('refresh_token', res.data.refresh_token);
                setAuthData(res.data);
                reset();
                window.location.replace("/profile");
            })
            .catch(function (error) {
                setErrorMessages([error.response.data.message]);
            });
    };


    const { success } = useParams();
    useEffect(() => {
        if (success === 'success') {
            setSuccessMessages(['Your account is activated, now you can log in']);
        }
    }, []);

    return (
        <div className="row">

            <div className="col-6 mx-auto mt-5">
                <h2>Login</h2>
                {
                    successMessages.map(msg => (
                        <div className="alert alert-success" key={msg} role="alert">{msg}</div>
                    ))
                }
                {
                    errorMessages.map(error => (
                        <div className="alert alert-danger" key={error} role="alert">{error}</div>
                    ))
                }
                <form onSubmit={handleSubmit(onSubmitHandler)}>
                    <div className="mb-3">
                        <label htmlFor="email" className="form-label">Email address</label>
                        <input type="email" className="form-control" id="email" {...register("email")} required />
                        <p className='text-danger'>{errors.email?.message}</p>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="password" className="form-label">Password</label>
                        <input type="password" className="form-control" id="password" {...register("password")} />
                        <p className='text-danger'>{errors.password?.message}</p>
                    </div>
                    <div className="mb-3">
                        <Link to="/forgot-password" className="nav-link">Forgot password</Link>
                    </div>
                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>
            </div>

        </div>
    )
}

export default Login
