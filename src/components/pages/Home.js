import React, { useEffect, useState } from 'react'
import { useParams, useHistory } from "react-router-dom";
import axios from 'axios';
import { config } from '../../config';

const Home = () => {
    const [errorMessages, setErrorMessages] = useState([]);
    const { token } = useParams();
    let history = useHistory();

    useEffect(()=>{
        if (token) {
            axios({
                method: 'get',
                url: `${config.SERVER_URL}/api/auth/verification/${token}`,
                responseType: 'json'
            })
            .then(function (res) {
                console.log(res);
                history.push("/auth/activation/success");
            })
            .catch(function (error) {
                console.log(error);
                setErrorMessages([error.response.data?.message]);
            });
        }
    },[]);


    return (
        <div className="bg-light p-5 rounded">
            {
                errorMessages.map(error => (
                    <div className="alert alert-danger" key={error} role="alert">{error}</div>
                ))
            }
            <h1>Sit example</h1>
            <p className="lead">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi mollitia molestiae placeat quaerat in debitis modi rem corporis pariatur repudiandae ducimus explicabo atque assumenda reiciendis ex consectetur magni, quod quam?</p>
            <a className="btn btn-lg btn-primary" href="/docs/5.0/components/navbar/" role="button">View navbar docs »</a>
        </div>
    )
}

export default Home

