import React, {useContext} from 'react';
import { Link } from "react-router-dom";
import AuthContext from '../../context/AuthContext';

function NavBar() {
    const {auth} = useContext(AuthContext);

    const logoutHandler = () => {
        localStorage.removeItem("refresh_token");
        console.log("user has bean log out");
        window.location.replace("/login");
    }

    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
            <div className="container">
                <div className="col">
                    <a className="navbar-brand" href="#">SiteName</a> 
                    {auth.accessToken && <span className='welcome-user'>(Welcome, {auth.username})</span>}
                </div>
                <div className="col float-end">
                    <button className="navbar-toggler align-right" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse " id="navbarNavAltMarkup">
                        <div className="navbar-nav">
                            <Link to="/" className="nav-link" aria-current="page" >Home</Link>
                            {!auth.accessToken && <Link to="/registration" className="nav-link">Registration</Link>}
                            {!auth.accessToken && <Link to="/login" className="nav-link">Login</Link>}
                            {auth.accessToken && <Link to="/password-reset" className="nav-link">Password reset</Link>}
                            {auth.accessToken && <Link to="/profile" className="nav-link">Profile</Link>}
                            {auth.accessToken && <Link to="/profile" className="nav-link" onClick={logoutHandler}>Logout</Link>}
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    )
}

export default NavBar
